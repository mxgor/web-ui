'use client';

import LoginInterceptor from '@/components/login/LoginInterceptor';
import RegisterLogin from '@/components/login/RegisterLogin';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import AppContextProvider from './AppContextProvider';
import DiscoveryContext from './DiscoveryContext';
import TanStackProvider from './TanStackProvider';
import ParamsProvider from './ParamsProvider';

export const WalletProvider = ({ children }: { children: React.ReactNode }): JSX.Element => {
  return (
    <AppContextProvider>
      <TanStackProvider>
        <LoginInterceptor>
          <RegisterLogin>
            <DndProvider backend={HTML5Backend}>
              <DiscoveryContext>
                <ParamsProvider>{children}</ParamsProvider>
              </DiscoveryContext>
              {/* {children} */}
            </DndProvider>
          </RegisterLogin>
        </LoginInterceptor>
      </TanStackProvider>
    </AppContextProvider>
  );
};

export const AppProvider = ({ children }: { children: React.ReactNode }): JSX.Element => {
  return (
    <AppContextProvider>
      <TanStackProvider>
        <LoginInterceptor>{children}</LoginInterceptor>
      </TanStackProvider>
    </AppContextProvider>
  );
};
