'use client';

import { ReactKeycloakProvider } from '@react-keycloak/web';
import Keycloak, { type KeycloakError } from 'keycloak-js';
import KeycloakLoginInterceptor from './KeycloakLoginInterceptor';
import { useCallback, useContext, useEffect, useState } from 'react';
import { AppContext } from '@/store/AppContextProvider';
import LoadingSpinner from '../loading-spinner/LoadingSpinner';
import type { KeycloakAndMetadata } from '@/service/types';
import { useQueryClient } from '@tanstack/react-query';

interface RegisterLoginProps {
  children: React.ReactNode;
}

const RegisterLogin = ({ children }: RegisterLoginProps): JSX.Element => {
  const { setError } = useContext(AppContext);
  const [keycloak, setKeycloak] = useState<Keycloak>();
  const data = useQueryClient().getQueryData<KeycloakAndMetadata>(['keycloakConfigAndMetadata']);

  const handleError = useCallback(
    (error: KeycloakError) => {
      setError(Error(error.error_description));
    },
    [setError]
  );

  useEffect(() => {
    const fetchKeycloakConfig = async (): Promise<void> => {
      if (data) {
        const kc = new Keycloak({
          url: `${data.keycloakConfig.auth}/`,
          realm: data.keycloakConfig.realm,
          clientId: data.keycloakConfig.clientId,
        });

        kc.onAuthError = handleError;

        setKeycloak(kc);
      }
    };

    void fetchKeycloakConfig();
  }, [data, handleError]);

  return (
    <>
      {keycloak && (
        <ReactKeycloakProvider
          onEvent={(event, error) => console.log(event, error)}
          initOptions={{ onLoad: 'login-required', checkLoginIframe: false, pkceMethod: 'S256' }}
          authClient={keycloak}
          LoadingComponent={<LoadingSpinner />}
        >
          <KeycloakLoginInterceptor>{children}</KeycloakLoginInterceptor>
        </ReactKeycloakProvider>
      )}
    </>
  );
};

export default RegisterLogin;
