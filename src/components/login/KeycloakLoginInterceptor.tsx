'use client';

import { useKeycloak } from '@react-keycloak/web';
import { useAuth } from 'oidc-react';
import { useContext, useEffect } from 'react';
import { AppContext } from '@/store/AppContextProvider';
import LoadingSpinner from '../loading-spinner/LoadingSpinner';

interface KeycloakLoginInterceptorProps {
  children: React.ReactNode;
}

const KeycloakLoginInterceptor = ({ children }: KeycloakLoginInterceptorProps): JSX.Element => {
  const { initialized, keycloak } = useKeycloak();
  const auth = useAuth();
  const { setError } = useContext(AppContext);

  useEffect(() => {
    if (keycloak.authenticated && !auth.userData) {
      auth.signIn().catch(setError);
    }
  }, [keycloak.authenticated]);

  return (
    <>
      {!initialized && <LoadingSpinner />}
      {initialized && keycloak.authenticated && children}
    </>
  );
};

export default KeycloakLoginInterceptor;
