'use client';

import React, { useContext, useEffect, useState } from 'react';
import { AuthProvider, UserManager } from 'oidc-react';
import { useRouter } from 'next/navigation';
import type { KeycloakAndMetadata, KeycloakConfig, OidcMetadata } from '@/service/types';
import { genericFetch } from '@/service/apiService';
import { useQuery } from '@tanstack/react-query';
import { AppContext } from '@/store/AppContextProvider';
import LoadingSpinner from '../loading-spinner/LoadingSpinner';
import { useLocale } from 'next-intl';

interface LoginInterceptorProps {
  children: React.ReactNode;
}

const getKeycloakConfig = async (): Promise<KeycloakAndMetadata> => {
  const keycloakConfig = await genericFetch<KeycloakConfig>(
    process.env.API_URL_CONFIG_SERVICE ?? '/api/keycloak-config'
  );

  const metadata = await genericFetch<OidcMetadata>(
    `${keycloakConfig.auth}/realms/${keycloakConfig.realm}/.well-known/openid-configuration`
  );

  return {
    keycloakConfig,
    metadata,
  };
};

const LoginInterceptor = ({ children }: LoginInterceptorProps): JSX.Element => {
  const [userManager, setUserManager] = useState<UserManager>();
  const router = useRouter();
  const locale = useLocale();
  const { data, isLoading, error } = useQuery({ queryKey: ['keycloakConfigAndMetadata'], queryFn: getKeycloakConfig });
  const { setError } = useContext(AppContext);

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);

    if (urlParams.has('presentation')) {
      const presentationId = urlParams.get('presentation');
      localStorage.setItem('urlParams', JSON.stringify({ presentation: presentationId }));
    }

    if (urlParams.has('params')) {
      const params = JSON.parse(urlParams.get('params') as string);

      localStorage.setItem('pluginParams', JSON.stringify(params));
    }
  }, []);

  useEffect(() => {
    error &&
      process.env.NODE_ENV === 'development' &&
      setError(
        Error(
          `This is a friendly reminder to ensure that Keycloak is running properly and configured correctly for the seamless operation of our application.

            To assist you in this process, we have detailed instructions and helpful information in the repository's README file. Please take a moment to review the README file to ensure that Keycloak is set up according to the specified configuration.
            
            If you encounter any difficulties or have questions during the configuration process, don't hesitate to reach out for assistance.`
        )
      );

    error &&
      process.env.NODE_ENV !== 'development' &&
      setError(
        Error(
          'Sorry, we are unable to process your request at the moment. An unexpected error occurred with our authentication system (Keycloak). Please try again later. If the issue persists, contact our support team for assistance.'
        )
      );
  }, [error]);

  useEffect(() => {
    const fetchConfig = async (): Promise<void> => {
      if (!isLoading && data) {
        const userManager = new UserManager({
          authority: data.keycloakConfig.auth,
          redirect_uri: `${process.env.ENV_URL ?? 'http://localhost:3000'}/${locale}/wallet/credentials`,
          scope: 'openid profile',
          response_type: 'code',
          client_id: data.keycloakConfig.clientId,
          metadata: {
            ...data.metadata,
          },
        });

        setUserManager(userManager);
      }
    };

    void fetchConfig();
  }, [isLoading, data]);

  const onSignIn = (): void => {
    router.push(`/${locale}/wallet/credentials`);
  };

  return (
    <>
      {isLoading && <LoadingSpinner />}
      {userManager && (
        <AuthProvider
          autoSignIn={false}
          userManager={userManager}
          onSignIn={onSignIn}
          loadUserInfo={false}
        >
          {children}
        </AuthProvider>
      )}
    </>
  );
};

export default LoginInterceptor;
