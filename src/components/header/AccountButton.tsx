'use client';

import { useAuth } from 'oidc-react';
import css from './Header.module.scss';
import SearchUserLoginState from './SearchUserLoginState';
import useLogoutApi from '@/hooks/useLogoutApi';
import { useRouter } from 'next/navigation';
import { useContext } from 'react';
import { AppContext } from '@/store/AppContextProvider';
import { useQueryClient } from '@tanstack/react-query';
import type { KeycloakAndMetadata } from '@/service/types';
import { useLocale, useTranslations } from 'next-intl';

const AccountButton = (): JSX.Element => {
  const auth = useAuth();
  const data = useQueryClient().getQueryData<KeycloakAndMetadata>(['keycloakConfigAndMetadata']);
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const [logOut] = useLogoutApi(data!.keycloakConfig, auth.userData?.access_token, auth.userData?.refresh_token);
  const router = useRouter();
  const locale = useLocale();
  const { setError } = useContext(AppContext);
  const t = useTranslations('Auth');

  const handleSignIn = (): void => {
    auth.signIn().catch(setError);
  };

  const handleSignOut = (): void => {
    const signOut: Promise<void> = auth.signOut();
    const logOutPromise: Promise<void> = logOut();

    void Promise.all([signOut, logOutPromise])
      .then(() => {
        router.push(`/${locale}/`);

        window.location.reload();
      })
      .catch(setError);
  };

  return (
    <div className={css['flex-center']}>
      {!auth.userData && (
        <SearchUserLoginState
          text={t('login')}
          onClick={handleSignIn}
        />
      )}
      {auth.userData && (
        <SearchUserLoginState
          text={t('logout')}
          onClick={handleSignOut}
        />
      )}
    </div>
  );
};

export default AccountButton;
