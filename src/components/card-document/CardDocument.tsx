'use client';

import type { ValidCredential, Description } from '@/service/types';
import { formatDateString } from '@/utils/dateUtils';
import { useTranslations } from 'next-intl';
import { useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useDrag } from 'react-dnd';
import CardModal from '../card-modal/CardModal';
import css from './CardDocument.module.scss';

export interface CardDocumentProps {
  credential: ValidCredential;
  description: Description;
  metadata?: any; // define metadata type when available in the API
  movable?: boolean;
  id: string;
}

export function getTitle(credential: ValidCredential): string {
  if (Array.isArray(credential.type)) {
    return credential.type[1] ?? credential.type[0];
  } else {
    // @ts-expect-error
    return credential.type ?? credential.Type ?? credential.vct;
  }
}

const CardDocument = ({ credential, description, id, movable = false }: CardDocumentProps): JSX.Element => {
  const t = useTranslations('CredentialsOverview');
  const [showModal, setShowModal] = useState(false);
  const [{ isDragging }, drag] = useDrag({
    type: 'credential',
    item: { credential, id, description },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });

  console.log('credentials', credential.credentialSubject);

  useEffect(() => {
    if (isDragging) {
      console.log('dragging');
      setShowModal(false);
    }
  }, [isDragging]);

  const renderCredential = (): JSX.Element => {
    return (
      <div
        className={`${css['card-document']} shadow ${movable ? css.movable : ''}`}
        onClick={handleCardDetails}
      >
        <Container className="d-grid gap-3">
          <Row className="p-1 flex-nowrap">
            <Col className="d-flex flex-column justify-content-center align-items-start">
              <h2 className="display-6">{getTitle(credential)}</h2>
              <strong>
                {credential.issuanceDate && (
                  <span>
                    {t('issued-at')} {formatDateString(credential.issuanceDate)}
                  </span>
                )}
              </strong>
            </Col>
          </Row>

          <div>
            {credential &&
              Object.entries(credential.credentialSubject ?? {})
                ?.slice(0, 5)
                .map(([key, value]) => (
                  <Row
                    key={key}
                    className="flex-nowrap"
                  >
                    <Col>
                      <span data-label={key}>{key}:</span>
                    </Col>
                    <Col>
                      <strong>
                        {typeof value === 'object' ? (
                          <div dangerouslySetInnerHTML={{ __html: value.name }}></div>
                        ) : (
                          value
                        )}
                      </strong>
                    </Col>
                  </Row>
                ))}
          </div>
        </Container>
      </div>
    );
  };

  const handleCloseModal = (): void => {
    setShowModal(false);
  };

  const handleCardDetails = (): void => {
    setShowModal(true);
  };

  return (
    <>
      {movable ? <div ref={drag}>{renderCredential()}</div> : renderCredential()}

      <CardModal
        show={showModal}
        handleClose={handleCloseModal}
        data={credential}
        description={description}
        title={getTitle(credential)}
      />
    </>
  );
};

export default CardDocument;
