'use client';

import type { CredentialInPresentation, ValidCredential, VerifiableCredentials } from '@/service/types';
import CardDocument from '../card-document/CardDocument';
import css from './CredentialColumn.module.scss';
import PresentationModal from '../presentation-modal/PresentationModal';
import { useState } from 'react';

interface CredentialColumnProps {
  credential: VerifiableCredentials;
  movable?: boolean;
  presentation?: CredentialInPresentation;
}

const CredentialColumn = ({ credential, movable = false, presentation }: CredentialColumnProps): JSX.Element => {
  const [showModal, setShowModal] = useState(false);

  const credentialKeys = Object.keys(credential.credentials);
  const columns: string[][] = [];

  for (let i = 0; i < credentialKeys.length; i += 5) {
    columns.push(credentialKeys.slice(i, i + 5));
  }

  const handlePresentationClick = (presentation: CredentialInPresentation): void => {
    setShowModal(true);
  };

  return (
    <>
      {columns.map((column, columnIndex) => (
        <div
          key={columnIndex}
          className={`${css['card-column']} ${movable ? css.presentation : ''}`}
        >
          {!movable && (
            <div
              className={`${css['column-header']} ${presentation ? css['cursor-pointer'] : ''}`}
              onClick={presentation ? () => handlePresentationClick(presentation) : undefined}
            >
              <h2 className="text-white">{credential.description.name ?? credential.description.id}</h2>
            </div>
          )}
          <div className={`p-2 ${css['card-container']}`}>
            {column.map(key => (
              <CardDocument
                key={key}
                credential={credential.credentials[key] as ValidCredential}
                description={credential.description}
                id={key}
                movable={movable}
              />
            ))}
          </div>
        </div>
      ))}

      {presentation && (
        <PresentationModal
          show={showModal}
          handleClose={() => setShowModal(false)}
          data={presentation}
          title={credential.description.name ?? credential.description.id}
        />
      )}
    </>
  );
};

export default CredentialColumn;
