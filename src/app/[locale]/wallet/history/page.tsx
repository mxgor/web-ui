'use client';

import Table, { type TableData } from '@/components/table/Table';
import { useApiData } from '@/service/apiService';
import type { DefaultConfig, HistoryData } from '@/service/types';
import { AppContext } from '@/store/AppContextProvider';
import { useTranslations } from 'next-intl';
import { useAuth } from 'oidc-react';
import { useContext, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import css from './history.module.scss';
import { useQueryClient } from '@tanstack/react-query';
import { formatDateString } from '@/utils/dateUtils';

const History = (): JSX.Element => {
  const { userData } = useAuth();
  const { data, error, isLoading } = useApiData<HistoryData>(
    'historyList',
    `${process.env.API_URL_ACCOUNT_SERVICE}/history/list`,
    {
      headers: {
        Authorization: `Bearer ${userData?.access_token}`,
      },
    }
  );
  const config = useQueryClient().getQueryData<DefaultConfig>(['defaultConfig']);
  const [tableData, setTableData] = useState<TableData>();
  const [historyLength, setHistoryLength] = useState<number>(0);
  const { setError } = useContext(AppContext);
  const t = useTranslations('History');

  useEffect(() => {
    error && setError(error);
  }, [error]);

  useEffect(() => {
    if (!data || data.events.length <= 0) return;

    const historyData = data.events;

    setHistoryLength(historyData.length);

    setTableData({
      head: ['event', 'type', 'userId', 'timestamp'],
      body: historyData
        .map(({ ...history }, i) => {
          return {
            ...history,
            id: `id_${i}`,
            timestamp: formatDateString(history.timestamp),
          };
        })
        .sort((a, b) => (a.timestamp > b.timestamp ? -1 : 1))
        .slice(0, config?.historyLimit),
    });
  }, [data]);

  return (
    <Container fluid>
      <Row className="mb-4">
        <Col
          md="6"
          sm="12"
          className={`${css['flex-center']} justify-content-between gap-2 mb-2`}
        >
          <div className="d-flex gap-2 align-items-center">
            <h1 className="mb-0">{t('title')}</h1>
          </div>
        </Col>
      </Row>

      <Table
        data={tableData}
        isLoading={isLoading}
      ></Table>

      {tableData && (
        <div className="d-flex justify-content-end">
          <span>
            {t('length', {
              length: tableData?.body.length,
              total: historyLength,
            })}
          </span>
        </div>
      )}
    </Container>
  );
};

export default History;
