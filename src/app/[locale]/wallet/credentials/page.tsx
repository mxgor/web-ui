'use client';

import { Col, Container, Row } from 'react-bootstrap';
import css from './wallet.module.scss';
import Divider from '@/components/divider/Divider';
import { useContext, useEffect, useState } from 'react';
import { type VerifiableCredentials } from '@/service/types';
import { genericFetch, useApiData } from '@/service/apiService';
import { AppContext } from '@/store/AppContextProvider';
import { useTranslations } from 'next-intl';
import { useAuth } from 'oidc-react';
import CredentialColumn from '@/components/credential-column/CredentialColumn';
import SearchButton from '@/components/search-button/SearchButton';
import { debounce } from '@/utils/timeUtils';
import LoadingSpinner from '@/components/loading-spinner/LoadingSpinner';
import NoData from '@/components/no-data/NoData';

const Wallet = (): JSX.Element => {
  const { userData } = useAuth();
  const { data, error, isLoading } = useApiData<VerifiableCredentials[]>(
    'credentialList',
    `${process.env.API_URL_ACCOUNT_SERVICE}/credentials/list`,
    {
      headers: {
        Authorization: `Bearer ${userData?.access_token}`,
      },
    }
  );
  const { setError } = useContext(AppContext);
  const t = useTranslations('CredentialsOverview');
  const [filteredCredentials, setFilteredCredentials] = useState<VerifiableCredentials[]>([]);

  useEffect(() => {
    error && setError(error);
  }, [error]);

  useEffect(() => {
    if (!data) return;

    setFilteredCredentials(data);
  }, [data]);

  const handleOnSearch = debounce((searchValue: string): void => {
    const getFilteredCredentials = async (): Promise<VerifiableCredentials[]> => {
      return await genericFetch<VerifiableCredentials[]>(`${process.env.API_URL_ACCOUNT_SERVICE}/credentials/list`, {
        headers: {
          Authorization: `Bearer ${userData?.access_token}`,
        },
        body: JSON.stringify({
          search: searchValue,
        }),
        method: 'POST',
      });
    };

    getFilteredCredentials()
      .then(data => {
        setFilteredCredentials(data);
      })
      .catch(error => {
        setError(error);
      });
  }, 500);

  return (
    <Container fluid>
      <Row>
        <Col className={`${css['flex-center']} justify-content-between gap-2 mb-2`}>
          <div className="d-flex gap-2 align-items-center">
            <h1 className="mb-0">{t('title')}</h1>
          </div>
        </Col>
        <Col className={`${css['flex-center']} justify-content-end gap-2`}>
          <SearchButton onSearch={handleOnSearch} />
        </Col>
      </Row>

      <Divider className="my-2" />

      <div className={css['cards-container']}>
        {filteredCredentials && !isLoading ? (
          filteredCredentials.map(credential => (
            <CredentialColumn
              key={credential.description.id}
              credential={credential}
            />
          ))
        ) : isLoading ? (
          <div className={`${css['flex-center']}`}>
            <LoadingSpinner />
          </div>
        ) : (
          <div className={`${css['flex-center']} w-100`}>
            <NoData />
          </div>
        )}
      </div>
    </Container>
  );
};

export default Wallet;
