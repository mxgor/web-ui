'use client';

import { Button, Container } from 'react-bootstrap';
import Table, { type TableData } from '@/components/table/Table';
import type { DidData } from '@/service/types';
import { genericFetch, useApiData } from '@/service/apiService';
import { useContext, useEffect, useState } from 'react';
import { AppContext } from '@/store/AppContextProvider';
import { useAuth } from 'oidc-react';
import { formatDateString } from '@/utils/dateUtils';
import { useTranslations } from 'next-intl';
import CreateDidModal from '@/components/create-did-modal/CreateDidModal';
import { toast } from 'react-toastify';

const Did = (): JSX.Element => {
  const { userData } = useAuth();
  const { data, error, isLoading } = useApiData<DidData>(
    'didList',
    `${process.env.API_URL_ACCOUNT_SERVICE}/kms/did/list`,
    {
      headers: {
        Authorization: `Bearer ${userData?.access_token}`,
      },
    }
  );
  const [tableData, setTableData] = useState<TableData>();
  const { setError } = useContext(AppContext);
  const t = useTranslations('Did');
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    error && setError(error);
  }, [error]);

  useEffect(() => {
    if (!data || data.list.length <= 0) return;

    const didData = data.list;

    setTableData({
      head: ['id', 'did', 'timestamp'],
      body: didData.map(({ ...did }) => {
        return {
          ...did,
          timestamp: formatDateString(did.timestamp),
        };
      }),
    });
  }, [data]);

  const handleCreateDidOnSubmit = (keyType: string): void => {
    const createDid = async (): Promise<void> => {
      await genericFetch(`${process.env.API_URL_ACCOUNT_SERVICE}/kms/did/create`, {
        headers: {
          Authorization: `Bearer ${userData?.access_token}`,
        },
        body: JSON.stringify({
          keyType,
        }),
        method: 'POST',
      });
    };

    createDid()
      .then(() => {
        window.location.reload();
        toast.success(t('create-success'));
      })
      .catch(error => setError(error));
  };

  return (
    <>
      <Container fluid>
        <div className={`d-flex justify-content-between gap-2 mb-4`}>
          <h1 className="mb-0">{t('title')}</h1>

          <Button onClick={() => setShowModal(true)}>{t('add')}</Button>
        </div>

        <Table
          data={tableData}
          isLoading={isLoading}
          showId
        ></Table>
      </Container>

      <CreateDidModal
        show={showModal}
        handleClose={() => setShowModal(false)}
        onSubmit={handleCreateDidOnSubmit}
      />
    </>
  );
};

export default Did;
